# README #

The goal of this task is to develop application that can compare the content of two files.
Input parameters for the application are paths of two files (it’s a plus if application supports more than two files)
The result of program is show the difference between the files. The format of result is shown below.

The first column shows line number regarding the first file.
The second column shows:
* in case if line has changed
– in case line exists in the first file but does not exist in the second one 
+ in case line does not exist in the first file but exists in the second one
Nothing if line has not changed.
The third column shows:
If line has changed, it shows text in the following format:
Line content from the first file | line content from the second file
If line exists in the first file but is missing in the second file – show the line content
If the line is added in the second file – show the line content
For more examples, please check http://www.diffnow.com/. 

From the code prospective, program should be implemented as a component that can be reused in any other applications easily.