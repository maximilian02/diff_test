/**
 * Main application file
 */

//Imports
var http = require('http'),
    customDiff = require('custom_diff'),
    files = {
      file1: 'file1.txt',
      file2: 'file2.txt'
    };

//Process Files
/*
 * If in the future we need to add more than two files just add a 
 * new key e.g 'file3.txt' and put the path on the value.
 * Currently IS NOT supported. 
*/

var html = customDiff(files, function(htmlResponse) {
  var server = http.createServer(function (req, res) {
    res.writeHead(200, {
      'Content-Type': 'text/html',
      'Content-Length': htmlResponse.length
    });

    res.end(htmlResponse);
  });

  server.listen(9000, function() {
    console.log('Server listening on port: 9000');
  });
});